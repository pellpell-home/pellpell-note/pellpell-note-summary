import appendDialyHeaderContents from "./lib/append_dialy_header_contents.js";

const main = async () => {
    const headingNumber = 3;
    const headingTitle = "TOEIC";

    // "YYYY-MM-DD" or null
    const start_date = "2022-08-24";
    const end_date = null;

    await appendDialyHeaderContents(
        headingNumber,
        headingTitle,
        start_date,
        end_date,
    )
}

export default main;
