import { databaseId, targetProperty } from "./const.js";
import notion from "./notion_setup.js";

/**
 * 
 * @param {string | null} start_date 
 * @param {string | null} end_date 
 * @returns 
 */
const getFilter = (start_date, end_date) => {
    let filter = [
        {
            property: targetProperty,
            date: {
                is_not_empty: true,
            },
        },
    ];
    if (start_date) {
        filter.push(
            {
                property: targetProperty,
                date: {
                    on_or_after: start_date,
                },
            },
        )
    }
    if (end_date) {
        filter.push(
            {
                property: targetProperty,
                date: {
                    on_or_before: end_date,
                },
            },
        )
    }

    return filter;
}

/**
 * 
 * @param {string | null} start_date 
 * @param {string | null} end_date 
 * @returns 
 */
const getDialyPageIds = async (start_date, end_date) => {
    const filter = getFilter(start_date, end_date);

    const dialyPages = await notion.databases.query({
        database_id: databaseId,
        filter: {
            and: filter,
        },
        sorts: [
            {
                property: targetProperty,
                direction: 'ascending',
            },
        ],
    });

    const dialyPageIds = dialyPages.results.map(result => result.id);
    return dialyPageIds;
}

export default getDialyPageIds;
