import notion from "./notion_setup.js";

/**
 * 
 * @param {string} blockId 
 * @param {any[]} blockList 
 */
const appendBlocks = async (blockId, blockList) => {
    const limitBlocks = 100;
    for (let i = 0; i < blockList.length; i += limitBlocks) {
        await notion.blocks.children.append({
            block_id: blockId,
            children: blockList.slice(i, i + limitBlocks),
        });
    }

}

export default appendBlocks;
