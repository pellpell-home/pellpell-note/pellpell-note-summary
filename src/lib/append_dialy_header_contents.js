import getPageHeadingContent from "./get_page_heading_content.js";
import { outputPageId } from "./const.js";
import getDialyPageIds from "./get_dialy_page_ids.js"
import appendBlocks from "./append_blocks.js";

const appendDialyHeaderContents = async (headingNumber, headingTitle, start_date = null, end_date = null) => {
    const dialyPageIds = await getDialyPageIds(start_date, end_date);

    let uploadBlocks = [];

    for (let i = 0; i < dialyPageIds.length; i++) {
        const pageHeadingContent = await getPageHeadingContent(
            dialyPageIds[i],
            headingNumber,
            headingTitle
        );
        uploadBlocks = uploadBlocks.concat(pageHeadingContent);
    }

    await appendBlocks(outputPageId, uploadBlocks);
}

export default appendDialyHeaderContents;
