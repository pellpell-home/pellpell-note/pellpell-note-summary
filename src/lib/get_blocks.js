import notion from "./notion_setup.js"

/**
 * 
 * @param {string} blockId 
 * @returns
 */
const getBlocks = async (blockId) => {
    const response = await notion.blocks.children.list({
        block_id: blockId,
    });
    for (let i = 0; i < response.results.length; i++) {
        const block = response.results[i];
        if (!block.has_children) {
            continue;
        }
        const children = await getBlocks(block.id);
        block[block.type].children = children.results;
    }
    return response;
}

export default getBlocks;
