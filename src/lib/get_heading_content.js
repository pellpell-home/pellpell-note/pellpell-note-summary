import getBlocks from "./get_blocks.js"

/** `headingNumber` と `headingTitle` が一致する最初のヘッダーの後のブロックリスト
 * 
 * @param {string} blockId 
 * @param {number} headingNumber 
 * @param {string} headingTitle 
 * @returns 
 */
const getBlocksAfterHeading = (blocks, headingNumber, headingTitle) => {
    const headingType = "heading_" + headingNumber.toString();

    // headingType が一致しているブロックを取得
    const headingNumberBlocks = blocks.results.filter(result => result.type === headingType);

    // headingType と headingTitle が一致しているブロックを取得
    const headingTitleBlocks = headingNumberBlocks.filter(block => block[headingType].rich_text[0].plain_text === headingTitle);

    if (headingTitleBlocks.length == 0) {
        return null;
    }

    // headingType と headingTitle が一致している最初のブロックを取得
    const headingTitleBlock = headingTitleBlocks[0];

    const headingTitleBlockIndex = blocks.results.indexOf(headingTitleBlock);

    if (!headingTitleBlockIndex || headingTitleBlockIndex + 2 >= blocks.results.length) {
        return null;
    }

    return blocks.results.slice(headingTitleBlockIndex + 1);
}

/** `headingNumber` が一致する最初のヘッダーのインデックス
 * 
 * @param {any[] | null} blocksList
 * @param {number} headingNumber
 * @returns {number}
 */
const getBlocksHeadingNumberIndex = (blocksList, headingNumber) => {
    if (!blocksList) {
        return Number.MAX_SAFE_INTEGER;
    }

    const headingType = "heading_" + headingNumber.toString();

    // headingType が一致しているブロックを取得
    const headingNumberBlocks = blocksList.filter(result => result.type === headingType);

    if (headingNumberBlocks.length == 0) {
        return Number.MAX_SAFE_INTEGER;
    }

    // headingType が一致している最初のブロックを取得
    const headingTitleBlock = headingNumberBlocks[0];

    const headingTitleBlockIndex = blocksList.indexOf(headingTitleBlock);

    if (!headingTitleBlockIndex) {
        return Number.MAX_SAFE_INTEGER;
    }

    return headingTitleBlockIndex;
}

/** `headingNumber` 以下の最初ヘッダーの前のブロックリスト
 * 
 * @param {any[] | null} blocksList
 * @param {number} headingNumber
 * @returns {number}
 */
const getBlocksBeforeHeadingNumber = (blocksList, headingNumber) => {
    if (!blocksList) {
        return [];
    }

    let minNumber = Number.MAX_SAFE_INTEGER;

    for (let i = 1; i <= headingNumber; i++) {
        let index = getBlocksHeadingNumberIndex(blocksList, i);
        if (minNumber > index) {
            minNumber = index;
        }
    }

    if (minNumber === Number.MAX_SAFE_INTEGER) {
        return blocksList;
    }

    return blocksList.slice(0, minNumber);
}


/**
 * 
 * @param {string} blockId 
 * @param {number} headingNumber 
 * @param {string} headingTitle 
 * @returns 
 */
const getHeadingContent = async (blockId, headingNumber, headingTitle) => {
    const blocks = await getBlocks(blockId);

    const blocksAfterHeading = getBlocksAfterHeading(blocks, headingNumber, headingTitle);
    const headingContent = getBlocksBeforeHeadingNumber(blocksAfterHeading, headingNumber);

    return headingContent;
}

export default getHeadingContent;
