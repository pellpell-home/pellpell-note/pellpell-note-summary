import appendBlocks from "./append_blocks.js";
import { targetProperty } from "./const.js";
import getHeadingContent from "./get_heading_content.js";
import notion from "./notion_setup.js";

const getPropertyValue = async (inputPageId, targetProperty) => {
    const page = await notion.pages.retrieve({ page_id: inputPageId });
    const propertyId = page.properties[targetProperty].id;
    const propertyObject = await notion.pages.properties.retrieve({ page_id: inputPageId, property_id: propertyId });
    if (propertyObject.type === "date") {
        return propertyObject[propertyObject.type].start;
    }
    return null;
}

const getPropertyBlock = (propertyValue) => {
    return {
        "paragraph": {
            "rich_text": [
                {
                    "text": {
                        "content": propertyValue,
                    },
                    "annotations": {
                        "bold": true,
                    },
                }
            ]
        }
    }
}

/** 特定のページのヘッダーの内容を取得する
 * 
 * @param {string} inputPageId 
 * @param {number} headingNumber 
 * @param {string} headingTitle
 * @returns 
 */
const getPageHeadingContent = async (inputPageId, headingNumber, headingTitle) => {
    const propertyValue = await getPropertyValue(inputPageId, targetProperty);
    if (!propertyValue) {
        return null;
    }
    const propertyBlock = getPropertyBlock(propertyValue);
    const headingContent = await getHeadingContent(inputPageId, headingNumber, headingTitle);
    if (!headingContent) {
        return null;
    }

    const pageHeadingContent = [propertyBlock].concat(headingContent);

    return pageHeadingContent;
}

export default getPageHeadingContent;
